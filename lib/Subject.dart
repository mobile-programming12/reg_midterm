import 'package:flutter/material.dart';
class SubjecctTable extends StatelessWidget {
  const SubjecctTable({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.amber[50],
        appBar: AppBar(
          title: const Text('SUBJECT SCHEDULE', style: TextStyle(fontFamily: 'ChakraPetch', fontSize: 26, fontWeight: FontWeight.w300)),
          backgroundColor: Colors.grey[800],
        ),
        body: ListView(
          children:<Widget> [
            Container(
              padding: EdgeInsets.only(top: 10, left: 8),
              child: Text('วิชาที่เปิดสอน', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 25, color: Colors.amber[800]),),
            ),
            Column(
                children:<Widget> [
                  Container(
                      padding: EdgeInsets.only(top: 10, left: 8),
                      child: Text('โปรดระบุรหัสวิชา', style: TextStyle(fontFamily: 'Thai', fontSize: 22),)
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5,left: 80, right: 80),
                    child: TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'รหัสวิชา'
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    child: TextButton(onPressed: (){},
                      child: Text('ค้นหา', style: TextStyle( fontFamily: 'Thai', fontSize: 22, color: Colors.black)),),
                  )
                ]
            ),
          ],
        )
    );
  }
}