import 'package:flutter/material.dart';

class CalendarTable extends StatelessWidget {
  const CalendarTable({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[50],
        appBar: AppBar(
          title: const Text('CALENDAR', style: TextStyle(fontFamily: 'ChakraPetch', fontSize: 26, fontWeight: FontWeight.w300)),
          backgroundColor: Colors.grey[800],
        ),
        body: Column(
          children: [
            buildText(),
            Calendar(),
          ],
        )
      );
  }
}

class Calendar extends StatelessWidget {
  const Calendar({super.key});

  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.all(),
      columnWidths: const <int, TableColumnWidth>{
        0: FlexColumnWidth(5),
        1: FlexColumnWidth(4),
        2: FlexColumnWidth(4),
      },
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          decoration: const BoxDecoration(
            color: Colors.grey,
          ),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 8),
              child: Text('รายการ', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 22), textAlign: TextAlign.center,),
            ),
            Container(
              padding: EdgeInsets.only(top: 8),
              child: Text('วันเริ่มต้น', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 22), textAlign: TextAlign.center,),
            ),
            Container(
              padding: EdgeInsets.only(top: 8),
              child: Text('วันสิ้นสุด', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 22), textAlign: TextAlign.center,),
            ),
          ],
        ),
        TableRow(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('- ช่วงจองรายวิชาลงทะเบียนให้กับนิสิตโดยเจ้าหน้าที่', style: TextStyle(fontFamily: 'Thai', fontSize: 18)),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('1 พ.ย. 2565 \n8:30 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('18 พ.ย. 2565 \n23:59 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
          ],
        ),
        TableRow(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('- ลงทะเบียนปกติ ( on-line )', style: TextStyle(fontFamily: 'Thai', fontSize: 18)),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('11 พ.ย. 2565 \n8:30 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('18 พ.ย. 2565 \n23:59 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
          ],
        ),
        TableRow(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('- ชั้นปี 4', style: TextStyle(fontFamily: 'Thai', fontSize: 18)),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('11 พ.ย. 2565 \n8:30 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('18 พ.ย. 2565 \n23:59 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
          ],
        ),
        TableRow(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('- ชั้นปีอื่นๆ', style: TextStyle(fontFamily: 'Thai', fontSize: 18)),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('11 พ.ย. 2565 \n8:30 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('18 พ.ย. 2565 \n23:59 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
          ],
        ),
        TableRow(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('- ชั้นปี 3', style: TextStyle(fontFamily: 'Thai', fontSize: 18)),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('14 พ.ย. 2565 \n8:30 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('18 พ.ย. 2565 \n23:59 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
          ],
        ),
        TableRow(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('- ชั้นปี 2', style: TextStyle(fontFamily: 'Thai', fontSize: 18)),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('15 พ.ย. 2565 \n8:30 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('18 พ.ย. 2565 \n23:59 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
          ],
        ),
        TableRow(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('- ชั้นปี 1', style: TextStyle(fontFamily: 'Thai', fontSize: 18)),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('16 พ.ย. 2565 \n8:30 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
            Container(
              padding: EdgeInsets.only(left: 5),
              child: Text('18 พ.ย. 2565 \n23:59 น.่', style: TextStyle(fontFamily: 'Thai', fontSize: 18),textAlign: TextAlign.center,),
            ),
          ],
        ),
      ],
    );
  }
}
Widget buildText(){
  return Column(
    children: <Widget>[
      Container(
        padding: EdgeInsets.only(top: 8),
        child: Text('ปฏิทินการศึกษา', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 22, color: Colors.amber[800])),
      ),
      Row(
        children: [
          Container(
            padding: EdgeInsets.only(left: 8),
            child: Text('ปีการศึกษา 2/2565', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 20)),
          ),
          Container(
            child: IconButton(onPressed: (){}, icon: Icon(Icons.arrow_drop_down_outlined),),
          )
        ],
      ),
    ],
  );
}
