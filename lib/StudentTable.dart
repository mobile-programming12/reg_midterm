import 'package:flutter/material.dart';
class StudentTable extends StatelessWidget {
  const StudentTable({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.amber[50],
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('STUDENT SCHEDULE', style: TextStyle(fontFamily: 'ChakraPetch', fontSize: 26, fontWeight: FontWeight.w300)),
          backgroundColor: Colors.grey[800],
        ),
        body: ListView(
          children: [
            Container(
              padding: EdgeInsets.only(left: 8, top: 8),
              child: Text('ตารางเรียนนิสิต',style: TextStyle(fontFamily: 'ThaiBold', fontSize: 25, color: Colors.amber[800]),),
            ),
            SingleChildScrollView(
              child: buildTable(),
            )
          ],
        )



        // ListView(
        //   children:<Widget> [
        //     Container(
        //       padding: EdgeInsets.only(top: 10),
        //       child: Text('คำเตือน', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 30, color: Colors.red[600]),textAlign: TextAlign.center,),
        //     ),
        //     Container(
        //       // padding: EdgeInsets.only(top: 10),
        //       child: Text('ระบบไม่อนุญาตให้ท่านใช้งานในส่วนที่ร้องขอ อาจมีสาเหตุจาก', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 25, color: Colors.red[600]),textAlign: TextAlign.center,),
        //     ),
        //     Container(
        //       padding: EdgeInsets.only(left: 10),
        //       child: Text('1.ท่านยังไม่ได้เข้าสู่ระบบ '
        //           '\n2.ท่านเข้าสู่ระบบเรียบร้อยแล้ว หากท่านไม่ได้ใช้งานระบบนานเกิน 15 นาที ท่านจึงถูกให้ออกจากระบบโดยอัตโนมัติ '
        //           '\nกรุณาเข้าสู่ระบบโดยเลือกจากเมนูด้านซ้ายมือ...', style: TextStyle(fontFamily: 'Thai', fontSize: 20),),
        //     )
        //   ],
        // )
    );
  }
}
Widget buildTable(){
  return Center(
      child: Column(children: <Widget>[
        Container(
          margin: EdgeInsets.all(20),
          child: Table(
            columnWidths: const <int, TableColumnWidth>{
              0: FlexColumnWidth(4),
              1: FlexColumnWidth(4),
              2: FlexColumnWidth(4),
              // 0: FixedColumnWidth(150),
              // 2: FixedColumnWidth(80)
            },
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            // defaultColumnWidth: FixedColumnWidth(120.0),
            border: TableBorder.all(
                color: Colors.black,
                style: BorderStyle.solid,
                width: 2),
            children: [
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  children: [
                    Column(children:[Text('DAY', style: TextStyle(fontSize: 20.0))]),
                    Column(children:[Text('SUBJECTS', style: TextStyle(fontSize: 20.0))]),
                    Column(children:[Text('TIME', style: TextStyle(fontSize: 20.0))]),
                  ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.yellow[200],
                  ),
                  children: [
                Column(children:[Text('MONDAY')]),
                Column(children:[Text('\n- 88624559-59 \n(3)2, IF-4M210 \nIF '
                    '\n\n- 88624459-59 \n(3)2, IF-3M210 \nIF '
                    '\n\n- 88624359-59 \n(3)2, IF-4M210 \nIF' ,textAlign: TextAlign.center,)]),
                Column(children:[Text('10:00-12:00 \n\n\n\n13:00-15:00 \n\n\n\n17:00-19:00')]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.pink[100],
                  ),
                  children: [
                Column(children:[Text('TUESDAY')]),
                Column(children:[Text('\n- 88634259-59 \n(3)2, IF-4C02 \nIF '
                    '\n\n- 88624559-59 \n(3)2, IF-3C03 \nIF '
                    '\n\n- 88624459-59 \n(3)2, IF-4C01 \nIF',textAlign: TextAlign.center,)]),
                Column(children:[Text('10:00-12:00 \n\n\n\n13:00-15:00 \n\n\n\n17:00-19:00')]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.green[200],
                  ),
                  children: [
                Column(children:[Text('WEDNESDAY')]),
                Column(children:[Text('\n- 88634459-59 \n(3)2, IF-4C02 \nIF '
                    '\n\n- 88342559-59 \n(3)2, IF-4C01 \nIF '
                    '\n\n- 88624359-59 \n(3)2, IF-3C01 \nIF',textAlign: TextAlign.center,)]),
                Column(children:[Text('10:00-12:00 \n\n\n\n13:00-15:00 \n\n\n\n15:00-17:00')]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.orange[200],
                  ),
                  children: [
                Column(children:[Text('THURSDAY')]),
                Column(children:[Text('-')]),
                Column(children:[Text('-')]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.blue[100],
                  ),
                  children: [
                Column(children:[Text('FRIDAY')]),
                Column(children:[Text('\n- 88646259-59 \n(3)2, IF-5T05 \nIF '
                    '\n\n- 88634459-59 \n(3)2, IF-4C012 \nIF ',textAlign: TextAlign.center,)]),
                Column(children:[Text('09:00-12:00 \n\n\n\n13:00-15:00')]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.purple[100],
                  ),
                  children: [
                Column(children:[Text('SATURDAY')]),
                Column(children:[Text('-')]),
                Column(children:[Text('-')]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.red[200],
                  ),
                  children: [
                Column(children:[Text('SUNDAY')]),
                Column(children:[Text('-')]),
                Column(children:[Text('-')]),
              ]),
            ],
          ),
        ),
      ])
  );
}