import 'package:flutter/material.dart';
class Login extends StatelessWidget {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber[50],
      appBar: AppBar(
        title: const Text('LOGIN', style: TextStyle(fontFamily: 'ChakraPetch', fontSize: 26, fontWeight: FontWeight.w300)),
        backgroundColor: Colors.grey[800],
      ),
      body: ListView(
        children:<Widget> [
          Padding(padding: EdgeInsets.only(top: 20) ),
          buildLogo(),
          Padding(padding: EdgeInsets.only(top: 20) ),
          buildCard()
        ],
      )
    );
  }
}
Widget buildCard() {
  return Center(
      child: Card(
      elevation: 30,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
        child: SizedBox(
          width: 300,
          height: 270,
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 10),
                child: Text("LOGIN", textAlign: TextAlign.center, style: TextStyle(fontFamily: 'ChakraPetchBold', fontSize: 30),),
              ),
              Container(
                padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                child: TextField(decoration: InputDecoration(labelText: "ID CODE",
                    labelStyle: TextStyle(color: Colors.orange[900], fontFamily: 'ChakraPetch',fontSize: 25)),
                  style: TextStyle(fontSize: 20,fontFamily: 'ChakraPetch'),),
              ),
              Container(
                padding: EdgeInsets.only(top: 10, left: 10, right: 10),
                child: TextField(obscureText: true, decoration: InputDecoration(labelText: "PASSWORD",
                    labelStyle: TextStyle(color: Colors.orange[900], fontFamily: 'ChakraPetch',fontSize: 25))),
              ),
              Container(
                padding: EdgeInsets.only(top: 10, left: 80, right: 80),
                child: IconButton(onPressed: () {},icon: Icon(Icons.login_outlined, size: 40,color: Colors.green[700],),
                ),
              ),
            ],
          ),
        ),
      ),
  );
}
Widget buildLogo(){
  return Center(
    child: Column(
      children: <Widget>[
        Container(
          child: Image.asset('images/buulogo.png',height: 200,width: 200, fit: BoxFit.cover,),
        ),
        Container(
          padding: EdgeInsets.only(top: 15),
          child: Text('กรุณาป้อนรหัสประจำตัวและรหัสผ่าน', style: TextStyle(fontFamily: 'Thai', fontSize: 18,fontWeight: FontWeight.w800),),
        )
      ],
    )
  );
}