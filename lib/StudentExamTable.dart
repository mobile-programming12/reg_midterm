import 'package:flutter/material.dart';
class StudentExamTable extends StatelessWidget {
  const StudentExamTable({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.amber[50],
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('EXAM SCHEDULE', style: TextStyle(fontFamily: 'ChakraPetch', fontSize: 26, fontWeight: FontWeight.w300)),
          backgroundColor: Colors.grey[800],
        ),
        body: ListView(
          children: [
            Container(
              padding: EdgeInsets.only(left: 8, top: 8),
              child: Text('ตารางสอบกลางภาค',style: TextStyle(fontFamily: 'ThaiBold', fontSize: 25, color: Colors.amber[800]),),
            ),
            SingleChildScrollView(
              child: buildTable(),
            ),
            Divider(color: Colors.red,),
            Container(
              padding: EdgeInsets.only(left: 8, top: 8),
              child: Text('ตารางสอบปลายภาค',style: TextStyle(fontFamily: 'ThaiBold', fontSize: 25, color: Colors.amber[800]),),
            ),
            SingleChildScrollView(
              child: buildTable1(),
            ),
          ],
        )
    );
  }
}
Widget buildTable(){
  return Center(
      child: Column(children: <Widget>[
        Container(
          margin: EdgeInsets.all(20),
          child: Table(
            columnWidths: const <int, TableColumnWidth>{
              0: FlexColumnWidth(4),
              1: FlexColumnWidth(4),
              2: FlexColumnWidth(4),
              // 0: FixedColumnWidth(150),
              // 2: FixedColumnWidth(80)
            },
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            // defaultColumnWidth: FixedColumnWidth(120.0),
            border: TableBorder.all(
                color: Colors.black,
                style: BorderStyle.solid,
                width: 2),
            children: [
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  children: [
                    Column(children:[Text('ID SUBJECT', style: TextStyle(fontSize: 20.0))]),
                    Column(children:[Text('SUBJECTS', style: TextStyle(fontSize: 20.0))]),
                    Column(children:[Text('DATE-TIME', style: TextStyle(fontSize: 20.0))]),
                  ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.green[200],
                  ),
                  children: [
                Column(children:[Text('88624559-59')]),
                Column(children:[Text('- Software Testing',)]),
                Column(children:[Text('\n16/01/2022 \n 09:00-12:00 \n IF-3M210\n',textAlign: TextAlign.center,)]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.green[200],
                  ),
                  children: [
                Column(children:[Text('88624359-59')]),
                Column(children:[Text('- Web Programming',textAlign: TextAlign.center,)]),
                Column(children:[Text('\n16/01/2022 \n 17:00-20:00 \n IF-4C02\n',textAlign: TextAlign.center)]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.grey[100],
                  ),
                  children: [
                Column(children:[Text('88624459-59')]),
                Column(children:[Text('- Object-Oriented Analysis and Design',textAlign: TextAlign.center,)]),
                Column(children:[Text('\n17/01/2022 \n 17:00-20:00 \n IF-11M280\n',textAlign: TextAlign.center)]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.green[200],
                  ),
                  children: [
                Column(children:[Text('88634259-59')]),
                Column(children:[Text('- Multimedia Programming for Multiplatforms',textAlign: TextAlign.center)]),
                Column(children:[Text('\n18/01/2022 \n 13:00-16:00 \n IF-7T01\n',textAlign: TextAlign.center)]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.green[200],
                  ),
                  children: [
                Column(children:[Text('88646259-59')]),
                Column(children:[Text('- Introduction to Natural Language',textAlign: TextAlign.center,)]),
                Column(children:[Text('\n18/01/2022 \n 17:00-20:00 \n IF-11M280\n',textAlign: TextAlign.center)]),
              ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.grey[100],
                  ),
                  children: [
                Column(children:[Text('88634459-59')]),
                Column(children:[Text('- Mobile Application Development I',textAlign: TextAlign.center,)]),
                Column(children:[Text('\n20/01/2022 \n 13:00-16:00 \n ARR\n',textAlign: TextAlign.center)]),
              ]),
            ],
          ),
        ),
      ])
  );
}
Widget buildTable1(){
  return Center(
      child: Column(children: <Widget>[
        Container(
          margin: EdgeInsets.all(20),
          child: Table(
            columnWidths: const <int, TableColumnWidth>{
              0: FlexColumnWidth(4),
              1: FlexColumnWidth(4),
              2: FlexColumnWidth(4),
              // 0: FixedColumnWidth(150),
              // 2: FixedColumnWidth(80)
            },
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            // defaultColumnWidth: FixedColumnWidth(120.0),
            border: TableBorder.all(
                color: Colors.black,
                style: BorderStyle.solid,
                width: 2),
            children: [
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  children: [
                    Column(children:[Text('ID SUBJECT', style: TextStyle(fontSize: 20.0))]),
                    Column(children:[Text('SUBJECTS', style: TextStyle(fontSize: 20.0))]),
                    Column(children:[Text('DATE-TIME', style: TextStyle(fontSize: 20.0))]),
                  ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.green[200],
                  ),
                  children: [
                    Column(children:[Text('88624559-59')]),
                    Column(children:[Text('- Software Testing',)]),
                    Column(children:[Text('\n27/03/2023 \n 09:00-12:00 \n IF-3M210\n',textAlign: TextAlign.center,)]),
                  ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.green[200],
                  ),
                  children: [
                    Column(children:[Text('88624359-59')]),
                    Column(children:[Text('- Web Programming',textAlign: TextAlign.center,)]),
                    Column(children:[Text('\n27/03/2023 \n 17:00-20:00 \n IF-4C02\n',textAlign: TextAlign.center)]),
                  ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.grey[100],
                  ),
                  children: [
                    Column(children:[Text('88624459-59')]),
                    Column(children:[Text('- Object-Oriented Analysis and Design',textAlign: TextAlign.center,)]),
                    Column(children:[Text('\n28/03/2023 \n 17:00-20:00 \n IF-11M280\n',textAlign: TextAlign.center)]),
                  ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.green[200],
                  ),
                  children: [
                    Column(children:[Text('88634459-59')]),
                    Column(children:[Text('- Mobile Application Development I',textAlign: TextAlign.center,)]),
                    Column(children:[Text('\n29/03/2023 \n 09:00-12:00 \n IF-4C03\n',textAlign: TextAlign.center)]),
                  ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.green[200],
                  ),
                  children: [
                    Column(children:[Text('88634259-59')]),
                    Column(children:[Text('- Multimedia Programming for Multiplatforms',textAlign: TextAlign.center)]),
                    Column(children:[Text('\n29/03/2023 \n 13:00-16:00 \n IF-7T01\n',textAlign: TextAlign.center)]),
                  ]),
              TableRow(
                  decoration: BoxDecoration(
                    color: Colors.grey[100],
                  ),
                  children: [
                    Column(children:[Text('88646259-59')]),
                    Column(children:[Text('- Introduction to Natural Language',textAlign: TextAlign.center,)]),
                    Column(children:[Text('\n31/03/2023 \n 17:00-20:00 \n IF-11M280\n',textAlign: TextAlign.center)]),
                  ]),

            ],
          ),
        ),
      ])
  );
}