import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:reg_midterm/NavDrawer.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'REG SYSTEM  ',
        home: Scaffold(
          backgroundColor: Colors.amber[50],
          drawer: NavDrawer(),
          appBar: AppBar(
            centerTitle: false,
            title: const Text("BURAPHA UNIVERSITY", style: TextStyle(fontFamily: 'ChakraPetch', fontSize: 22, fontWeight: FontWeight.w300),),
            actions: [
              IconButton(onPressed: () {}, icon: Icon(Icons.translate_outlined))
            ],
            backgroundColor: Colors.grey[800],
            // title:
            // Stack(
            //   children: [
            //     Positioned(
            //         right: 10,
            //         child: Container(child:Image.asset('images/buulogo.png',height: 50,width: 50, fit: BoxFit.cover,) ,)
            //     ),
            //     Positioned(
            //         child: Text("BURAPHA UNIVERSITY", style: TextStyle(fontFamily: 'ChakraPetch', fontSize: 22, fontWeight: FontWeight.w300))
            //     ),
            //   ],),

            // actions: [
            //   Image(image: AssetImage('images/buulogo.png'))
            // ],
            //Image.asset('images/buusign.jpg', fit: BoxFit.fill, height: 100 ,)

            // toolbarHeight: 20,
            // actions: <Widget>[
            //   TextButton(onPressed: (){}, child: Text("LOGIN", style: TextStyle(color: Colors.white, fontSize: 15),)),
            //   // IconButton(onPressed: (){}, icon: Icon(Icons.login_outlined))
            // ],
          ),
            body: ListView(
              children: <Widget>[
                Stack( children: [
                  Container(
                    width: double.infinity,
                    height: 150,
                    child: Image( fit: BoxFit.cover,
                        image: AssetImage('images/buu4.png',)
                    ),
                  ),
                  Positioned(
                    top: 25,
                    left: 20,
                      child: Container(
                        child: Image.asset('images/buulogo.png',height: 100,width: 100, fit: BoxFit.cover,) ,) ,
                  ),
                  Positioned(
                    top: 50,
                    left: 125,
                    child: Container(
                      child: Text(' มหาวิทยาลัยบูรพา ' , style: TextStyle(fontSize: 30,color: Colors.yellow[700], fontFamily: 'ThaiBold',
                      shadows: [
                        Shadow(
                          offset: Offset(3, 0),
                          blurRadius: 0,
                          color: Color.fromARGB(255, 0, 0, 0),
                        ),
                      ]
                      )
                      ) ,
                    ) ,
                  ),
                  Positioned(
                      top: 5,
                      right: 10 ,
                      child: Container(
                        child: Image.asset('images/line.png',height: 40,width: 40, fit: BoxFit.cover,),
                      )),
                  Positioned(
                      top: 5,
                      right: 60 ,
                      child: Container(
                        child: Image.asset('images/facebook.png',height: 40,width: 40, fit: BoxFit.cover,),
                      ))
                ],),
                Container(
                  margin: EdgeInsets.only(top: 9, bottom: 0,left: 15,right: 0),
                  child: Text('ประกาศ', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 30),),
                ),
                Divider(color: Colors.red,),
                Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Text('1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565', style: TextStyle(fontFamily: 'Thai', fontSize: 22,fontWeight: FontWeight.w800),)
                ),
                Container(
                  child: Image.asset('images/noti1.png', fit: BoxFit.cover,),
                ),
                Divider(color: Colors.red,),
                Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Text('2.กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565', style: TextStyle(fontFamily: 'Thai', fontSize: 22,fontWeight: FontWeight.w800),)
                ),
                Container(
                  child: Image.asset('images/noti2.jpg', fit: BoxFit.cover,),
                ),
                Divider(color: Colors.red,),
                Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Text('3.แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี', style: TextStyle(fontFamily: 'Thai', fontSize: 22,fontWeight: FontWeight.w800),)
                ),
                Container(
                  child: Image.asset('images/noti3.jpg', fit: BoxFit.cover,),
                ),
              ],
            ),
        ),
      ),
    );
  }

}
