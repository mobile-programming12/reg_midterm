import 'package:flutter/material.dart';

class Statis extends StatelessWidget {
  const Statis({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.amber[50],
        appBar: AppBar(
          title: const Text('STATISTICS', style: TextStyle(fontFamily: 'ChakraPetch', fontSize: 26, fontWeight: FontWeight.w300)),
          backgroundColor: Colors.grey[800],
        ),
        body: ListView(
          children:<Widget> [
            Column(
              children:<Widget> [
                Padding(padding: EdgeInsets.only(top: 15)),
                Text('สถิติการเข้าศึกษาที่ BUU', style: TextStyle(fontFamily: 'ThaiBold', fontSize: 22, color: Colors.amber[800]),
              textAlign: TextAlign.center,)
              ],
            ),
            Column(
              children:<Widget> [
                Image.asset('images/graph.jpg', fit: BoxFit.cover,),
                Padding(padding: EdgeInsets.only(bottom: 15)),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children:<Widget> [
                // Padding(padding: EdgeInsets.only(top: 20)),
                build1(),
                build2(),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children:<Widget> [
                build3(),
                build4(),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children:<Widget> [
                build5(),
                build6(),
              ],
            ),

          ],
        )
    );
  }
}
Widget build1(){
  return Column(
    children: <Widget>[
      Image.asset('images/folder.png',height: 50,width: 50, fit: BoxFit.cover),
      Text('เอกสารการรับนิสิตปี 2560',style: TextStyle(fontFamily: 'Thai'), textAlign: TextAlign.center),
    ],
  );
}
Widget build2(){
  return Column(
    children: <Widget>[
      Image.asset('images/folder.png',height: 50,width: 50, fit: BoxFit.cover),
      Text('เอกสารการรับนิสิตปี 2561',style: TextStyle(fontFamily: 'Thai'), textAlign: TextAlign.center),
    ],
  );
}
Widget build3(){
  return Column(
    children: <Widget>[
      Image.asset('images/folder.png',height: 50,width: 50, fit: BoxFit.cover),
      Text('เอกสารการรับนิสิตปี 2562',style: TextStyle(fontFamily: 'Thai'), textAlign: TextAlign.center),
    ],
  );
}
Widget build4(){
  return Column(
    children: <Widget>[
      Image.asset('images/folder.png',height: 50,width: 50, fit: BoxFit.cover),
      Text('เอกสารการรับนิสิตปี 2563',style: TextStyle(fontFamily: 'Thai'), textAlign: TextAlign.center),
    ],
  );
}
Widget build5(){
  return Column(
    children: <Widget>[
      Image.asset('images/folder.png',height: 50,width: 50, fit: BoxFit.cover),
      Text('เอกสารการรับนิสิตปี 2564',style: TextStyle(fontFamily: 'Thai'), textAlign: TextAlign.center),
    ],
  );
}
Widget build6(){
  return Column(
    children: <Widget>[
      Image.asset('images/folder.png',height: 50,width: 50, fit: BoxFit.cover),
      Text('เอกสารการรับนิสิตปี 2565',style: TextStyle(fontFamily: 'Thai'), textAlign: TextAlign.center),
    ],
  );
}