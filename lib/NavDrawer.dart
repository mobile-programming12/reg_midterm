import 'package:flutter/material.dart';
import 'package:reg_midterm/CalendarTable.dart';
import 'package:reg_midterm/RoomTable.dart';
import 'package:reg_midterm/StudentTable.dart';
import 'package:reg_midterm/Subject.dart';
import 'package:reg_midterm/TeacherTable.dart';
import 'Login.dart';
import 'Statistics.dart';
import 'StudentExamTable.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              'WELCOME TO BUU',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 30, fontFamily: 'ChakraPetch'),
            ),
      decoration: BoxDecoration(
          color: Colors.black,
          image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage('images/buu2.jpg'))),
          ),
          ListTile(
            leading: Icon(Icons.login_outlined,size: 35,color: Colors.green[700]),
            title: Text('เข้าสู่ระบบ', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
            onTap: () => { Navigator.push(context, MaterialPageRoute(builder: (context)=> const Login()))},
            // tileColor: Colors.green,
          ),
          ListTile(
            leading: Icon(Icons.menu_book,size: 35,color: Colors.lightGreen),
            title: Text('วิชาที่เปิดสอน', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
            onTap: () => {Navigator.push(context, MaterialPageRoute(builder: (context)=> const SubjecctTable()))},
            // tileColor: Colors.green,
          ),
          ListTile(
            leading: Icon(Icons.table_chart,size: 35,color: Colors.grey[700]),
            title: Text('ตารางเรียนนิสิต', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
            onTap: () => {Navigator.push(context, MaterialPageRoute(builder: (context)=> const StudentTable()))},
          ),
          ListTile(
            leading: Icon(Icons.table_chart,size: 35,color: Colors.brown,),
            title: Text('ตารางสอบ', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
            onTap: () => {Navigator.push(context, MaterialPageRoute(builder: (context)=> const StudentExamTable()))},
          ),
          ListTile(
            leading: Icon(Icons.table_chart,size: 35,color: Colors.blueGrey[700]),
            title: Text('ตารางสอนอาจารย์', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
            onTap: () => {Navigator.push(context, MaterialPageRoute(builder: (context)=> const TeacherTable()))},
          ),
          ListTile(
            leading: Icon(Icons.calendar_month_outlined,size: 35,color: Colors.yellow[800],),
            title: Text('ปฏิทินการศึกษา', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
            onTap: () => {Navigator.push(context, MaterialPageRoute(builder: (context)=> const CalendarTable()))},
          ),
          // ListTile(
          //   leading: Icon(Icons.library_books_outlined,size: 35,color: Colors.blueAccent,),
          //   title: Text('แนะนำการลงทะเบียนเรียน', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
          //   onTap: () => {Navigator.of(context).pop()},
          // ),
          // ListTile(
          //   leading: Icon(Icons.money_outlined, size: 35,color: Colors.green,),
          //   title: Text('ค่าธรรมเนียมการศึกษา', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
          //   onTap: () => {Navigator.of(context).pop()},
          // ),
          // ListTile(
          //   leading: Icon(Icons.library_books_outlined,size: 35,color: Colors.blueAccent,),
          //   title: Text('แบบฟอร์ม', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
          //   onTap: () => {Navigator.of(context).pop()},
          // ),
          ListTile(
            leading: Icon(Icons.auto_graph_outlined, size: 35,color: Colors.deepOrange,),
            title: Text('สถิตินิสิต', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
            onTap: () => {Navigator.push(context, MaterialPageRoute(builder: (context)=> const Statis()))},
          ),
          ListTile(
            leading: Icon(Icons.logout_outlined,size: 35,color: Colors.red[600]),
            title: Text('ออกจากระบบ', style: TextStyle(fontSize: 20, fontFamily: 'Thai'),),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}
